//=============================================================================
// TIMIQCurl.h
//=============================================================================
// abstraction.......TimIQ low level access to TIMIQ hardware
// class.............TIMIQCurl
// original author...J. GOUNO - NEXEYA-FRANCE
//=============================================================================

#ifndef _TIMIQ_CURL_H_
#define _TIMIQ_CURL_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <iostream>

#if !defined (_SIMULATION_)	
#include <curl/curl.h>
#endif

#include <TIMIQTypesAndConsts.h>


namespace TIMIQLib_ns
{

//- TimIQ values structure
typedef struct timIQval
{
  // Tim IQ board values
  float temperature;
  float iVal;
  float qVal;
  float mixerSin;
  float mixerCos;
} timIQval_t;


	class TIMIQCurl
	{
		public:
		
		// Contructor 
		TIMIQCurl (const std::string& ip_address, const std::string& num_port);                   	
		
		// Destructor
		~TIMIQCurl();
			
		// Writes data on TimIQ equipment
		// @param data float value.	
		E_timiq_errno_t write_data(float& data);
			
		// Writes "I" tension on TimIQ equipment
		// @param iValue float value.	
		E_timiq_errno_t write_iValue(float& iValue);
			
		// Writes "Q" tension to TimIQ equipment
		// @param qValue float value.
		E_timiq_errno_t write_qValue(float& qValue);
			
		// Writes board temperature to TimIQ equipment
		// @param boardTemperature float value.
		E_timiq_errno_t write_boardTemperature(float& boardTemperature);
			
		// Writes command on TimIQ equipment
		// @param cmd TimIQCmd_t value.
    // @param thr thr = true: use threaded curl reference, otherwise, 
    // use standard curl reference
		E_timiq_errno_t write_command(E_timiq_cmd_t& cmd, bool thr = true);
		
		// Reads data from TimIQ equipment
		// @param [out] data float pointer.
		E_timiq_errno_t read_data(float& data);
		
		// Reads "I" value from TimIQ equipment 
		// @param [out] iValue float pointer.
		E_timiq_errno_t read_iValue(float& iValue);
		
		// Reads "Q" value from TimIQ equipment 
		// @param [out] qValue float pointer.
		E_timiq_errno_t read_qValue(float& qValue);
		
		// Reads the mixer cosinus output from TimIQ equipment 
		// @param [out] mixerCosOutput float pointer.
		E_timiq_errno_t read_mixerCosOutput(float& mixerCosOutput);
		
		// Reads the mixer sinus output from TimIQ equipment
		// @param [out] mixerSinOutput float pointer.
		E_timiq_errno_t read_mixerSinOutput(float& mixerSinOutput);
		
		// Reads the board temperature from TimIQ equipment
		// @param [out] boardTemperature float pointer.
		E_timiq_errno_t read_boardTemperature(float& boardTemperature);
	
		// Reads TimIQ state of TimIQ equipment
		// @param [out] state vector of string
		E_timiq_errno_t read_state_and_status(std::string& status, E_timiq_code_t& codeRet);
			
		// Reads all values (I, Q, mixer, temperature) from TimIQ equipment 
                // in a single command
		// @param [out] val TimIQ values structure.
		E_timiq_errno_t read_all(timIQval_t& values);

		// Gets error messages
		// @return string variable
		std::string get_err_msg(void)
		{
			return m_error_string;
		}
		
		private:
			//- internal members
			//-------------------
			//- IP address 
			std::string m_base_addr;
			
#if !defined (_SIMULATION_)	
			//- CURL easy handle
			CURL * m_hw_curl;

			//- CURL easy handle for threaded write actions
			CURL * m_thr_hw_curl;
#endif

#if defined (_SIMULATION_)
      double m_sim_ival;
      double m_sim_qval;
#endif
			// message error
			std::string m_error_string;
			
			//- internal functions
			//--------------------
			
			// ...
			
			
		protected:
			
			//- internal functions
			//--------------------
			// connects to api
			// connects to an easy handle using curl
      // @param thr thr = true: use threaded curl reference, otherwise, 
      // use standard curl reference
			// @return 0: no error, others: an internal error occurred
			E_timiq_errno_t connect_i(bool thr = false);
			
			
			// writes on timiq WebServer
			// @param url Url of the page
      // @param strData Data to send
      // @param thr thr = true: use threaded curl reference, otherwise, 
      // use standard curl reference
			// @return  true if success, false otherwise
			bool write_i(std::string& url, std::string& strData, bool thr = true);
			
			// reads float data from timiq WebServer
			// @param url of the page, strData data value to find, [out] float value to extract
			// @return  true if success, false otherwise
			bool read_float_i(const std::string& url, const std::string& strData, float& value );
			
			// reads timiq equipment state and status 
			// @param url of the page, string state
			// @return  true if success, false otherwise
			bool read_state_and_status_i(const std::string& url, std::string& state, E_timiq_code_t& codeRet );		

			// reads all values from timiq WebServer
			// @param url of the page, [out] extracted values
			// @return  true if success, false otherwise
			bool read_all_i(const std::string& url, timIQval_t& value );
			
#if !defined (_SIMULATION_)	
			// send data to webserver
			// @param url Url to use 
      // @param thr thr = true: use threaded curl reference, otherwise, 
      // use standard curl reference
			// @return CURLcode			
			CURLcode send_to_webserver_i(const std::string& url, bool thr = false);
#endif
			
		};
}
#endif //- _TIMIQ_CURL_H_