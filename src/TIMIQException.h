//=============================================================================
// TIMIQException.h
//=============================================================================
// abstraction.......Tim IQ Application Programming Interface
// class.............TIMIQ Error & Exception specification
// original author...xxxxxx
//=============================================================================

#ifndef _TIMIQ_EXCEPTION_H_
#define _TIMIQ_EXCEPTION_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <string>
#include <vector>

namespace TIMIQLib_ns {

// ============================================================================
// TIMIQ error severities 
// ============================================================================
typedef enum {
  WARN, 
  ERR, 
  PANIC
} Severity;

// ============================================================================
// The TIMIQ Error abstraction base class.  
//
// Contains 5 fields:
// � reason
// � description
// � origin
// � error code
// � severity
// ============================================================================
class Error
{
public:

  // Constructor. 
  Error ();

  // Constructor with parameters.
  Error (const char *_reason,
				 const char *_desc,
				 const char *_origin,
	       int _code = -1, 
	       TIMIQLib_ns::Severity _severity = TIMIQLib_ns::ERR);
  

  // Constructor with parameters.
  Error (const std::string& _reason,
				 const std::string& _desc,
				 const std::string& _origin, 
	       int _code = -1, 
	       TIMIQLib_ns::Severity _severity = TIMIQLib_ns::ERR);

  // Copy constructor.
  Error (const Error& _src);

  // Destructor.
  virtual ~Error ();

  // Operator=
  Error& operator= (const Error& _src);

  // Error details: reason
  std::string reason;

  // Error details: description
  std::string desc;

  // Error details: origin
  std::string origin;

  // Error details: code
  int code;

  // Error details: severity
  TIMIQLib_ns::Severity severity;
};

// ============================================================================
// The TIMIQ error list.	
// ============================================================================
typedef std::vector<Error> ErrorList;

// ============================================================================
// The TIMIQ Exception abstraction base class.  
//  
// Contains a list of TIMIQ Errors.
// 
// ============================================================================
class Exception
{
public:

  // Constructor.
  Exception ();

  // Constructor with parameters.
  Exception (const char *_reason,
					   const char *_desc,
					   const char *_origin,
	           int _code = -1, 
	           TIMIQLib_ns::Severity _severity = TIMIQLib_ns::ERR);
  
  // Constructor with parameters.
  Exception (const std::string& _reason,
					   const std::string& _desc,
					   const std::string& _origin, 
	           int _code = -1, 
	           TIMIQLib_ns::Severity _severity = TIMIQLib_ns::ERR);

  // Constructor from Error class.
  Exception (const Error& error);


  // Copy constructor.
  Exception (const Exception& _src);

  // Operator=
  Exception& operator= (const Exception& _src); 

  // Destructor.
  virtual ~Exception ();

  // Pushes the specified error into the errors list.
  void push_error (const char *_reason,
					         const char *_desc,
						       const char *_origin, 
		               int _code = -1, 
		               TIMIQLib_ns::Severity _severity = TIMIQLib_ns::ERR);

  // Pushes the specified error into the errors list.
  void push_error (const std::string& _reason,
                   const std::string& _desc,
                   const std::string& _origin, 
                   int _code = -1, 
                   TIMIQLib_ns::Severity _severity = TIMIQLib_ns::ERR);

  // Pushes the specified error into the errors list.
  void push_error (const Error& _error);

  // The error list.
  ErrorList errors;
};

} // namespace TIMIQLib_ns

#endif // _TIMIQ_EXCEPTION_H_

