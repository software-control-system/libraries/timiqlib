//=============================================================================
// TimIQTypesAndConsts.h
//=============================================================================
// abstraction.......Basic types and Constants for TimIQ library
// class.............Basic structures
// original author...J. GOUNO - NEXEYA-FRANCE
//=============================================================================

#ifndef _TIMIQ_TYPES_AND_CONSTS_H_
#define _TIMIQ_TYPES_AND_CONSTS_H_

//=============================================================================
// DEPENDENCIES
//=============================================================================

namespace TIMIQLib_ns	
{

// ============================================================================
// CONSTANTS
// ============================================================================

//- web protocol: http 
const std::string kHTTP_WEB_PROTOCOL = "http://";

//- Web page: data
//----------------------
const std::string kTIMIQ_DATA_PAGE= "/get_data.wsgi";
const std::string kTIMIQ_DATA_KEY= "data";

//- Web page: IValue
//----------------------
const std::string kTIMIQ_IVALUE_PAGE= "/set_iValue.wsgi";
const std::string kTIMIQ_IVALUE_KEY= "iValue";

//- Web page: QValue
//----------------------
const std::string kTIMIQ_QVALUE_PAGE= "/set_qValue.wsgi";
const std::string kTIMIQ_QVALUE_KEY= "qValue";

//- Web page: boardTemperature
//------------------------------
const std::string kTIMIQ_BOARD_TEMPERATURE_PAGE= "/set_boardTemperature.wsgi";
const std::string kTIMIQ_BOARD_TEMPERATURE_KEY= "boardTemperature";

//- Web page: command
//----------------------
const std::string kTIMIQ_COMMAND_PAGE= "/set_command.wsgi";
const std::string kTIMIQ_COMMAND_KEY= "command";

//- Web page: feedback
//-------------------------
const std::string kTIMIQ_FEEDBACK_PAGE= "/get_feedback.wsgi";

//- mixerCosOutput key
const std::string kTIMIQ_MIXER_COS_OUTPUT_KEY= "mixerCosOutput";

//- mixerSinOutput key
const std::string kTIMIQ_MIXER_SIN_OUTPUT_KEY= "mixerSinOutput";

//- Web page: state
//-------------------------
const std::string kTIMIQ_STATE_PAGE= "/get_state.wsgi";

//- message key
const std::string kTIMIQ_MESSAGE_KEY= "message=";
//- state key
const std::string kTIMIQ_STATE_KEY= "state=";
//- state code
const std::string kTIMIQ_CODE_KEY= "code=";

//- buffer key size
const size_t kMAX_DATA_SIZE = 50;

// ============================================================================
// TYPES
// ============================================================================
//- TimIQ library error numbers
typedef enum  {
  // No error occurred
  timiq_NO_ERROR = 0,

  // timIQ errors
  timiq_internal_ERROR  
}E_timiq_errno_t;

//- TimIQsSystem codes
typedef enum
{
	Error = 0,
	OK,	
	Busy,
	Warning,
	Undefined
} E_timiq_code_t;
 
//- TimIQsSystem commands
typedef enum
{
	RESET_SYSTEM = 1,
	RECALIBRATE_PLL,
	ACKNOWLEDGE_ERRORS
} E_timiq_cmd_t;

//------------------------------
//- timIQ struct config 
//------------------------------
// threaded action type
typedef enum
{
	Undef = 0,
	TI_IVAL,
	TI_QVAL,
	TI_BTEMP,
	TI_DATA,
	TI_CALIB
} E_ti_t;

typedef struct timIQConfig
{
	E_ti_t id;
	float value;
	E_timiq_errno_t ti_err;
	
	 //- default constructor -----------------------
	timIQConfig()
	: id(Undef),
	value(0.0),
	ti_err(timiq_NO_ERROR)
	{
	};
	
	//- destructor -----------------------
	~timIQConfig ()
	{
	}

	//- copy constructor ------------------
	timIQConfig (const timIQConfig& src)
	{
		*this = src;
	}

	//- operator= ------------------
	const timIQConfig & operator= (const timIQConfig& src)
	{
		if (this == & src) 
		  return *this;

		  this->id     = src.id;
		  this->value  = src.value;
		  this->ti_err = src.ti_err;
		  
		  return *this;
	}

	//- dump -----------------------
	void dump () const
	{
		std::cout <<  "timIQConfig::id........." 
		  << this->id
		  << std::endl;    
		std::cout <<  "timIQConfig::value........." 
		  << this->value
		  << std::endl;  
	}
}timIQConfig_t;
 
} //- namespace timiqlib

#endif //- _TIMIQ_TYPES_AND_CONSTS_H_

