//=============================================================================
// TIMIQLib.h
//=============================================================================
// abstraction.......Tim IQ Application Programming Interface
// class.............TIMIQLib 
// original author...J. GOUNO - NEXEYA-FRANCE
//=============================================================================

#ifndef _TIMIQ_LIB_H_
#define _TIMIQ_LIB_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <TIMIQException.h>
#include <TIMIQCurl.h>
#include <yat/threading/Task.h>



// ==========================================================================
// DEFINITION OF API
//
// This API allows access to an electronic equipment "TimIQ". 
// It allows a control software to send and receive data 
// via a http web protocol.
// ===========================================================================

namespace TIMIQLib_ns {

//------------------------------------------------------------------------
// Class Description:
// Thread to write the values and commands on the TimIQ equipment
//------------------------------------------------------------------------
class ThreadedAction : public yat::Thread
{
  friend class TIMIQLib;
	  
  protected:

    //- ctor ---------------------------------
    ThreadedAction(yat::Thread::IOArg ioa, timIQConfig& cfg);

    //- dtor ---------------------------------
    virtual ~ThreadedAction();

    //- thread's entry point
    virtual yat::Thread::IOArg run_undetached (yat::Thread::IOArg ioa);
	  
    //- asks this ThreadedAction to quit
    virtual void exit();
	  
    //- Thread state
    bool isThreadAlive()
    {
      return m_goOn;
    }	
		
    bool  isWriteDone()
    {
      return m_isActionDone; 
    }
		
  private:
		
    //- thread's ctrl flag
    bool m_goOn;
		
    //- indicates if the threaded action is done
    bool m_isActionDone;

    //- indicates if the threaded action has generated an error
    bool m_isActionInError;
		
    //- Thread action configuration member
    timIQConfig m_ti_cfg;
};

	
//- low layer TimIQ curl class
//-----------------------------
class TIMIQCurl;
	

//------------------------------------------------------------------------
//- TIMIQProxy Class
//- Ensures the interface of the timIQ equipment with 
//- a tango software control system 
//------------------------------------------------------------------------
class TIMIQLib
{
  friend class ThreadedAction;
		
  public:

    // Contructor 
    TIMIQLib();
    
    // Destructor
    ~TIMIQLib();
			
    //- Write functions
    //---------------------------------------------------------------------------
    // Sets data to TimIQ equipment
    // @param data float value.	
    void set_data(float data) 
      throw (Exception);
			
    // Regulates "I" tension to TimIQ equipment
    // @param iValue float value.	
    void set_iValue(float iValue)
      throw (Exception);
		
    // Regulates "Q" tension to TimIQ equipment
    // @param qValue float value.
    void set_qValue(float qValue)
      throw (Exception);
			
    // Sets board temperature to TimIQ equipment
    // @param boardTemperature float value.
    void set_boardTemperature(float boardTemperature) 
      throw (Exception);
			
    // Sets command to TimIQ equipment
    // @param cmd TimIQCmd_t value.
    void set_command(E_timiq_cmd_t& cmd) 
      throw (Exception);
				
    //- Read functions
    //---------------------------------------------------------------------------
    // Gets data from TimIQ equipment
    // @param [out] data float pointer.
    void get_data(float &data)
      throw (Exception);
			
    // Gets "I" value from TimIQ equipment 
    // @param [out] iValue float pointer.
    void get_iValue(float& iValue)
      throw (Exception);
			
    // Gets "Q" value from TimIQ equipment 
    // @param [out] qValue float pointer.
    void get_qValue(float& qValue)
      throw (Exception);
			
    // Gets the mixer cosinus output from TimIQ equipment 
    // @param [out] mixerCosOutput float pointer.
    void get_mixerCosOutput(float& mixerCosOutput)
      throw (Exception);
			
    // Gets the mixer sinus output from TimIQ equipment
    // @param [out] mixerSinOutput float pointer.
    void get_mixerSinOutput(float& mixerSinOutput)
      throw (Exception);
			
    // Gets the board temperature from TimIQ equipment
    // @param [out] boardTemperature float pointer.
    void get_boardTemperature(float& boardTemperature)
      throw (Exception);
		
    // Gets the whole data from TimIQ equipment
    // i.e. temperature, i & q values, mixer values
    // @param [out] val Read values.
    void get_boardData(timIQval_t& val)
      throw (Exception);
		
    // Gets state of TimIQ equipment
    // @param [out] status string
    E_timiq_code_t get_state(std::string& status) 
      throw (Exception);
			
    // Initialize  TimIQCurl library
    // @param ip_address string, port_number string				
    void init (const std::string& ip_address, const short& port_number) 
      throw (Exception);
			
  private:
	
    //- internal members
    //--------------------------
    TIMIQCurl * m_timiq_hw;
			
    //- ip address number
    std::string m_ipAddress;
	
    //- port number
    std::string m_numPort;
			
    //- internal timiq state
    E_timiq_code_t m_internal_timiq_state;
			
			
  protected:	

    //- internal functions
    //-------------------------
	
    //- manage command by using task
    ThreadedAction * m_timiq_task;
			
    // execute action specified in argument
    void ExecuteAction(timIQConfig & cfg)
     throw (Exception);

    // end of task notification
    void end_task()
      throw (Exception);

};

} // namespace TIMIQLib_ns

#endif // _TIMIQ_LIB_H_

