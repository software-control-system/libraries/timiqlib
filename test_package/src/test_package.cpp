#include <iostream>

#include <TIMIQLib.h>

int main()
{
    TIMIQLib_ns::TIMIQLib t = TIMIQLib_ns::TIMIQLib();
    float a = 0.;
    try { t.get_iValue(a); }
    catch (...) { }
    std::cout << a << '\n';

    return 0;
}
