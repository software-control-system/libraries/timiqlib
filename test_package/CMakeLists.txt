cmake_minimum_required(VERSION 3.15)
project(PackageTest CXX)

find_package(timiqlib CONFIG REQUIRED)
find_package(cpptango CONFIG REQUIRED)
find_package(yat CONFIG REQUIRED)
find_package(curl CONFIG REQUIRED)

add_executable(test_package src/test_package.cpp)
target_link_libraries(test_package timiqlib::timiqlib)
target_link_libraries(test_package yat::yat)
target_link_libraries(test_package cpptango::cpptango)
target_link_libraries(test_package curl::curl)

