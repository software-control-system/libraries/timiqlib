from conan import ConanFile

class timiqlibRecipe(ConanFile):
    name = "timiqlib"
    version = "1.0.1"
    package_type = "library"
    user = "soleil"
    python_requires = "base/[>=1.0]@soleil/stable"
    python_requires_extend = "base.Library"
    
    license = "GPL-2"
    author = "Gouno", "Buteau"
    url = "https://gitlab.synchrotron-soleil.fr/software-control-system/libraries/timiqlib"
    description = "TimIQ library"
    topics = ("utility", "control-system")

    settings = "os", "compiler", "build_type", "arch"
    options = {"shared": [True, False], "fPIC": [True, False]}
    default_options = {"shared": False, "fPIC": True}

    exports_sources = "CMakeLists.txt", "src/**", "include/**"

    def requirements(self):
        self.requires("cpptango/9.2.5@soleil/stable")
        self.requires("yat/[>=1.0]@soleil/stable")
        self.requires("curl/7.37.0@soleil/stable")
